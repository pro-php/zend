<?

namespace User\Form;

use Laminas\Form\Form;

class UserForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('user'); // form name

        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'login',
            'type' => 'text',
            'options' => [
                'label' => 'Login',
            ],
        ]);

        $this->add([
            'name' => 'first_name',
            'type' => 'text',
            'options' => [
                'label' => 'First Name',
            ],
        ]);

        $this->add([
            'name' => 'last_name',
            'type' => 'text',
            'options' => [
                'label' => 'Last Name',
            ],
        ]);

        $this->add([
            'name' => 'date_birth',
            'type' => 'text',
            'options' => [
                'label' => 'Date of birth',
            ],
        ]);

        $this->add([
            'name' => 'about',
            'type' => 'text',
            'options' => [
                'label' => 'About You',
            ],
        ]);

        $this->add([
            'name' => 'place',
            'type' => 'text',
            'options' => [
                'label' => 'Your Place',
            ],
        ]);

        $this->add([
            'name' => 'url',
            'type' => 'text',
            'options' => [
                'label' => 'Site URL',
            ],
        ]);

        $this->add([
            'name' => 'date_reg',
            'type' => 'text',
            'options' => [
                'label' => 'Date of Registration',
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Enter',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}