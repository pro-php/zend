<?
namespace Twitter\Controller;

use Twitter\Model\TwitterTable;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

//addAction
use Twitter\Form\TwitterForm;
use Twitter\Model\Twitter;

class TwitterController extends AbstractActionController
{
    private $table;

    public function __construct(TwitterTable $table)
    {
        $this->table = $table;
    }

    public function indexAction()
    {
        return new ViewModel([
            'twitts' => $this->table->fetchAll(),
        ]);
    }

    public function addAction()
    {
        $form = new TwitterForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return ['form' => $form];
        }

        $twitter = new Twitter();
        $form->setInputFilter($twitter->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return ['form' => $form];
        }

        $twitter->exchangeArray($form->getData());
        $this->table->saveTwitt($twitter);
        return $this->redirect()->toRoute('twitter');
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (0 === $id) {
            return $this->redirect()->toRoute('twitter', ['action' => 'add']);
        }

        try {
            $twitt = $this->table->getTwitt($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('twitter', ['action' => 'index']);
        }

        $form = new TwitterForm();
        $form->bind($twitt); //������� ������ getArrayCopy model
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if (! $request->isPost()) {
            return $viewData;
        }

        $form->setInputFilter($t->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return $viewData;
        }

        $this->table->saveTwitt($twitt);

        return $this->redirect()->toRoute('twitter', ['action' => 'index']);
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('twitter');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->table->deleteTwitt($id);
            }

            return $this->redirect()->toRoute('twitter');
        }

        return [
            'id'    => $id,
            'twitt' => $this->table->getTwitt($id),
        ];
    }

}