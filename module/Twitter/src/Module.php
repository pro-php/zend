<?
namespace Twitter;

use Laminas\Db\Adapter\AdapterInterface;
use Laminas\Db\ResultSet\ResultSet;
use Laminas\Db\TableGateway\TableGateway;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    // getConfig() method is here
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    // Add this method:
    public function getServiceConfig()
    {
        return [
            'factories' => [
                Model\TwitterTable::class => function($container) {
                    $tableGateway = $container->get(Model\TwitterTableGateway::class);
                    return new Model\TwitterTable($tableGateway);
                },
                Model\TwitterTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Twitter());
                    return new TableGateway('twitter', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\TwitterController::class => function($container) {
                    return new Controller\TwitterController(
                        $container->get(Model\TwitterTable::class)
                    );
                },
            ],
        ];
    }
}