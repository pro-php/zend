<?

namespace Twitter\Form;

use Laminas\Form\Form;

class TwitterForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('twitter'); // form name

        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);

        $this->add([
            'name' => 'login',
            'type' => 'text',
            'options' => [
                'label' => 'Login',
            ],
        ]);

        $this->add([
            'name' => 'twitt',
            'type' => 'text',
            'options' => [
                'label' => 'Twitt it',
            ],
        ]);

        $this->add([
            'name' => 'date_add',
            'type' => 'text',
            'options' => [
                'label' => 'Date added',
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}