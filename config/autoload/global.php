<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
//https://www.youtube.com/watch?v=07h3OBIF9kc
//use Laminas\Db\Adapter;

return [
    'db' => [
        'driver' => 'Pdo',
//        'dsn'    => sprintf('sqlite:%s/data/laminastutorial.db', realpath(getcwd())),
	'dsn'    => 'mysql:dbname=zend2;host=localhost;charset=utf8',

            'username' => 'zend2',
            'password' => '123',
            'driver_options' => array(
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''), // optional
//	'dsn'    => 'mysql:host=localhost;dbname=zend2,zend2,123;charset=utf8',
    ],
];
