--
-- Структура таблицы `twitter`
--

CREATE TABLE `twitter` (
  `id` int(11) NOT NULL,
  `login` varchar(100) NOT NULL DEFAULT '0',
  `twitt` varchar(280) DEFAULT NULL,
  `date_add` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- ССЫЛКИ ТАБЛИЦЫ `twitter`:
--

--
-- Дамп данных таблицы `twitter`
--

INSERT INTO `twitter` (`id`, `login`, `twitt`, `date_add`) VALUES
(1, 'first', '2020 Laminas Project a Series of LF Projects, LLC.2020 Laminas Project a Series of LF Projects, LLC.', '05.05.2020'),
(2, 'user2', 'Twitter      Home     Album     Users     TwitterTwitter      Home     Album     Users     Twitter', '06.05.2020');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `date_birth` varchar(100) DEFAULT NULL,
  `about` varchar(100) DEFAULT NULL,
  `place` varchar(100) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `date_reg` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- ССЫЛКИ ТАБЛИЦЫ `user`:
--

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `first_name`, `last_name`, `date_birth`, `about`, `place`, `url`, `date_reg`) VALUES
(1, 'first', 'name', 'last name', '10.11.1980', 'page1', 'Russia', 'http://ya.ru', '09.05.2020'),
(2, 'user2', 'name', 'last', '31.11.1982', 'page2', 'Moscow', 'http://moscow.ru', '02.05.2020');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `twitter`
--
ALTER TABLE `twitter`
  ADD PRIMARY KEY (`login`) USING BTREE,
  ADD KEY `id` (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `twitter`
--
ALTER TABLE `twitter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

